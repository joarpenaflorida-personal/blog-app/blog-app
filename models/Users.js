const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "Username is required."],
  },
  email: {
    type: String,
    required: [true, "Email is required."],
  },
  password: {
    type: String,
    required: [true, "Password is required."],
    select: false,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },

  likes: [
    {
      userId: {
        type: String,
        required: [true, "User Id is required"],
      },
      blogId: {
        type: String,
        required: [true, "User ID is required"],
      },
    },
  ],

  blogs: [
    {
      blogId: {
        type: String,
        required: [true, "Blog Id is required"],
      },
      blogTitle: {
        type: String,
        required: [true, "Blog Title is required"],
      },
      postedOn: {
        type: Date,
        default: Date.now,
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
