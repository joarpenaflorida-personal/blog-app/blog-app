const mongoose = require("mongoose");

const blogSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Username is required."],
  },
  description: {
    type: String,
    required: [true, "Email is required."],
  },
  readTime: {
    type: String,
    required: [true, "Read Time is required."],
  },
  slug: {
    type: String,
    required: [true, "Slug is required."],
  },
  author: {
    type: String,
    required: [true, "Author is required."],
  },
  tag: {
    type: String,
    required: [true, "Tag is required."],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  userId: {
    type: String,
    required: [true, "User ID is required."],
  },
  postedOn: {
    type: Date,
    default: Date.now,
  },
  likes: [
    {
      userId: {
        type: String,
        required: [true, "User Id is required"],
      },
    },
  ],

  comments: [
    {
      comment: {
        type: String,
        required: [true, "Comment is required"],
      },
      username: {
        type: String,
        required: [true, "Username is required"],
      },

      blogId: {
        type: String,
        required: [true, "Blog ID is required"],
      },
      commentedOn: {
        type: Date,
        default: Date.now,
      },
    },
  ],
});

module.exports = mongoose.model("Blog", blogSchema);
