const express = require("express");
const auth = require("../auth");

const userControllers = require("../controllers/userControllers");

const router = express.Router();

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/register", userControllers.registerUser);
router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getProfile);

router.get("/user/all", auth.verify, userControllers.fetchAllUsers);
router.get("/user/admin", auth.verify, userControllers.fetchAllAdmin);
router.get("/user/", auth.verify, userControllers.fetchNonAdmin);

router.patch("/:userId", auth.verify, userControllers.updateUserStatus);

router.put("/:userId", auth.verify, userControllers.updateUserProfile);

module.exports = router;
