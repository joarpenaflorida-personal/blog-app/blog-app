const express = require("express");
const auth = require("../auth");

const blogControllers = require("../controllers/blogControllers");

const router = express.Router();

// Add Blog
router.post("/", auth.verify, blogControllers.addBlog);
// Update Status Blog
router.patch("/:blogId", auth.verify, blogControllers.archiveBlog);
// Update Blog
router.put("/:blogId", auth.verify, blogControllers.updateBlog);
// Delete Blog
router.delete("/:blogId", auth.verify, blogControllers.deleteBlog);

// Fetch
router.get("/blog/all", auth.verify, blogControllers.fetchAllBlog);
router.get("/blog/", auth.verify, blogControllers.fetchActiveBlog);
router.get("/blog/archive", auth.verify, blogControllers.fetchNonActiveBlog);
router.get("/blog/user", auth.verify, blogControllers.fetchUserBlogs);
router.get("/blog/search/tag", auth.verify, blogControllers.fetchByTag);
router.get("/blog/:blogId", auth.verify, blogControllers.fetchSpecificBlog);

// comments
router.post("/comment/:blogId", auth.verify, blogControllers.addComment);

router.get("/comment/:blogId", auth.verify, blogControllers.fetchComments);

router.put(
  "/comment/delete/:slug/:commentId",
  auth.verify,
  blogControllers.deleteComment
);

router.put(
  "/comment/update/:slug/:commentId",
  auth.verify,
  blogControllers.updateComment
);

router.post("/blog/like/:blogId", auth.verify, blogControllers.toggleLike);

module.exports = router;
