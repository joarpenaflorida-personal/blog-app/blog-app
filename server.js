const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const blogRoutes = require("./routes/blogRoutes");

const app = express();

mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-bootcamp.m9kt1ev.mongodb.net/blog-posts?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to the cloud database."));

const corsOpts = {
  origin: "*",
  credentials: true,
  methods: ["GET", "POST", "HEAD", "PUT", "PATCH", "DELETE"],
  allowedHeaders: ["Content-Type"],
  exposedHeaders: ["Content-Type"],
};

app.use(cors(corsOpts));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/blogs", blogRoutes);

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`API running at port ${port}.`));
