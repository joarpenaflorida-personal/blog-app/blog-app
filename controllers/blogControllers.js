const auth = require("../auth");

const Blog = require("../models/Blog");
const User = require("../models/Users");

module.exports.addBlog = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const slugify = require("slugify");
  const { title, description, tag } = req.body;

  const wpm = 200;
  let textLength = description.split(" ").length;
  const result = Math.ceil(textLength / wpm);
  let official = "";
  if (result <= 59) {
    official = `${result}m read`;
  }
  if (result >= 60) {
    official = `${result}hr read`;
  }
  let slug = slugify(title, {
    replacement: "-",
    remove: undefined,
    lower: false,
    strict: false,
    locale: "vi",
    trim: true,
  });

  let newBlog = new Blog({
    description: description,
    slug: slug,
    title: title,
    readTime: official,
    author: userData.username,
    tag: tag,
    userId: userData.id,
  });

  if (userData) {
    let blogId = "";
    const isBlogUpdated = await Blog.find({ title: title }).then((blog) => {
      return newBlog
        .save()
        .then((blog) => {
          blogId = blog._id;
          return true;
        })
        .catch((err) => {
          console.log(err);
          return false;
        });
    });

    const isUserUpdated = await User.findById(userData.id)
      .then((user) => {
        user.blogs.push({
          blogTitle: title,
          blogId: blogId,
        });
        return user.save().then((result) => {
          console.log(result);
          return true;
        });
      })
      .catch((err) => {
        console.log(err);
        return false;
      });

    isBlogUpdated && isUserUpdated ? res.send(true) : res.send(false);
  } else {
    res.status(403).send("Unauthorized user.");
  }
};

module.exports.archiveBlog = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData) {
    const { isActive } = req.body;
    const { blogId } = req.params;
    let archiveBlog = {
      isActive: isActive,
    };
    await Blog.findByIdAndUpdate(blogId, archiveBlog, { new: true })
      .then((result) => {
        res.send(true);
      })
      .catch((err) => res.send(false));
  } else {
    res.status(403).send("Unauthorized user.");
  }
};
module.exports.deleteBlog = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const { blogId } = req.params;
  if (userData) {
    await Blog.findByIdAndRemove(blogId)
      .then((result) => res.send(true))
      .catch((err) => res.send(err));
  } else {
    res.status(403).send("Unauthorized user.");
  }
  module.exports.updateBlog = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const slugify = require("slugify");
    const { title, description, tag } = req.body;
    const { blogId } = req.params;

    const wpm = 200;
    let textLength = description.split(" ").length;
    const result = Math.ceil(textLength / wpm);
    let official = "";
    if (result <= 59) {
      official = `${result}m read`;
    }
    if (result >= 60) {
      official = `${result}hr read`;
    }
    let slug = slugify(title, {
      replacement: "-",
      remove: undefined,
      lower: false,
      strict: false,
      locale: "vi",
      trim: true,
    });
    if (userData) {
      let updatedBlog = {
        description: description,
        slug: slug,
        title: title,
        readTime: official,
        author: userData.username,
        tag: tag,
      };

      await Blog.findByIdAndUpdate(blogId, updatedBlog, { new: true })
        .then((result) => res.send(true))
        .catch((err) => res.send(err));
    }
  };
  module.exports.fetchUserBlogs = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData) {
      await Blog.find({ userId: userData.id })
        .then((result) => {
          if (result.length > 0) {
            res.send(result);
          } else {
            res.send({ message: "No Blog found." });
          }
        })
        .catch((err) =>
          res.status(404).send({ message: "Something went wrong." })
        );
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };
  module.exports.fetchAllBlog = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData) {
      await Blog.find({})
        .sort({ postedOn: -1 })
        .then((result) => {
          if (result.length > 0) {
            res.send(result);
          } else {
            res.send({ message: "No Blog found." });
          }
        })
        .catch((err) =>
          res.status(404).send({ message: "Something went wrong." })
        );
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };

  module.exports.fetchActiveBlog = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData) {
      await Blog.find({ isActive: true })
        .sort({ postedOn: -1 })
        .then((result) => {
          if (result.length > 0) {
            res.send(result);
          } else {
            res.send({ message: "No Blog found." });
          }
        })
        .catch((err) =>
          res.status(404).send({ message: "Something went wrong." })
        );
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };

  module.exports.fetchNonActiveBlog = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData) {
      await Blog.find({ isActive: false })
        .sort({ postedOn: -1 })
        .then((result) => {
          if (result.length > 0) {
            res.send(result);
          } else {
            res.send({ message: "No Blog found." });
          }
        });
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };

  module.exports.fetchSpecificBlog = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { slug } = req.params;
    if (userData) {
      await Blog.findOne({ slug: slug })
        .then((result) => res.send(result))
        .catch((err) => res.send(false));
    }
  };

  module.exports.fetchByTag = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { tag } = req.body;

    if (userData) {
      await Blog.find({ tag: tag })
        .then((result) => {
          if (result.length < 1) {
            res.send("No Blog found.");
          }
          res.send(result);
        })
        .catch((err) => res.send("Something went wrong."));
    } else {
      res.status(403).send("Unauthorized user");
    }
  };

  module.exports.fetchComments = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { blogId } = req.params;

    if (userData) {
      await Blog.findById(blogId)
        .then((blog) => {
          if (blog.comments.length === 0) {
            res.send({ message: "No comments found." });
          }
          res.send(blog.comments);
        })
        .catch((err) => res.send("Something went wrong."));
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };

  module.exports.addComment = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { comment } = req.body;
    const { blogId } = req.params;
    let userId = userData.id;
    let username = userData.username;
    console.log(userData);
    if (userData) {
      let isCommentAdded = await Blog.findById(blogId).then((blog) => {
        blog.comments.push({
          comment: comment,
          username: username,
          blogId: blogId,
          userId: userId,
        });
        return blog
          .save()
          .then((result) => {
            console.log(result);
            return true;
          })
          .catch((err) => {
            console.log(err);
            return false;
          });
      });
      isCommentAdded && res.status(201).send(true);
    } else {
      res.send(false);
    }
  };

  module.exports.updateComment = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { comment } = req.body;
    const { commentId, slug } = req.params;

    if (userData) {
      await Blog.findOneAndUpdate(
        {
          slug: slug,
          "comments._id": commentId,
        },
        {
          $set: { "comments.$.comment": comment },
        }
      )
        .then((result) => {
          res.send(true);
        })
        .catch((err) => {
          res.send(false);
        });
    } else {
      res.status(403).send("Unauthorized user.");
    }
  };

  module.exports.deleteComment = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const { slug, commentId } = req.params;
    console.log("hello");
    if (userData) {
      await Blog.findOneAndUpdate(
        {
          slug: slug,
        },
        {
          $pull: {
            comments: { _id: commentId },
          },
        }
      )
        .then((result) => {
          res.send(true);
        })
        .catch((err) => {
          res.send(false);
        });
    } else {
      res.send("Unauthorized user.");
    }
  };

  module.exports.toggleLike = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let user = {
      id: userData.id,
    };
    const { blogId } = req.params;

    console.log(user.id);
    console.log(blogId);

    if (userData) {
      let blog = await Blog.findById(blogId);
      if (blog) {
        let blogIndex = blog.likes.findIndex((l) => l.userId === user.id);
        if (blogIndex > -1) {
          blog.likes.pop({ userId: user.id });

          blog = await blog
            .save()
            .then((result) => res.send(blog))
            .catch((err) => res.send(err));
        } else {
          blog.likes.push({ userId: user.id });

          blog = await blog
            .save()
            .then((result) => res.send(blog))
            .catch((err) => res.send(err));
        }
      }
    } else {
      res.send("Unauthorized User");
    }
  };
};
