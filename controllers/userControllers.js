const User = require("../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const validator = require("validator");

module.exports.registerUser = async (req, res) => {
  return await User.find({ email: req.body.email }).then((result) => {
    const { email, password, username } = req.body;

    const passwordValidate =
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    const isPasswordValid = passwordValidate.test(password);
    const isEmailValid = validator.isEmail(email);
    if (isPasswordValid && isEmailValid) {
      let emailLower = email.toLowerCase();

      let newUser = new User({
        email: emailLower,
        password: bcrypt.hashSync(password, 10),
        username: username,
      });
      return newUser
        .save()
        .then((result) => {
          res.send(true);
        })
        .catch((err) => {
          res.send(false);
        });
    }
  });
};

module.exports.checkEmailExists = (req, res) => {
  const { email } = req.body;
  return User.find({ email: email })
    .then((result) => {
      if (result.length > 0) {
        res.send(true);
      } else {
        return res.send(false);
      }
    })
    .catch((err) => res.send(err));
};

module.exports.getProfile = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  return await User.findById(userData.id)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(false);
    });
};

module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email }).then((result) => {
    if (result === null) {
      return res.send({ message: "No user found!" });
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );
      if (isPasswordCorrect) {
        return res.send({
          accessToken: auth.createAccessToken(result),
        });
      } else {
        return res.status(403).send({ message: "Incorrect Password" });
      }
    }
  });
};

module.exports.fetchAllUsers = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return await User.find({}).then((result) => {
      res.send(result);
    });
  } else {
    res.status(403).send("Unauthorized User.");
  }
};
module.exports.fetchAllAdmin = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return await User.find({ isAdmin: true }).then((result) => {
      res.send(result);
    });
  } else {
    res.status(403).send("Unauthorized User.");
  }
};
module.exports.fetchNonAdmin = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    return await User.find({ isAdmin: false }).then((result) => {
      res.send(result);
    });
  } else {
    res.status(403).send("Unauthorized User.");
  }
};

module.exports.updateUserStatus = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    const { isAdmin } = req.body;
    let updateStatus = {
      isAdmin: isAdmin,
    };
    return await User.findByIdAndUpdate(req.params.userId, updateStatus)
      .then((result) => {
        res.send(true);
      })
      .catch((err) => {
        res.send(false);
      });
  } else {
    res.status(401).send("Unauthorized user.");
  }
};

module.exports.updateUserProfile = async (req, res) => {
  const { password, username } = req.body;
  const { userId } = req.params;
  const userData = auth.decode(req.headers.authorization);

  const passwordValidate =
    /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
  const isPasswordValid = passwordValidate.test(password);

  if (isPasswordValid) {
    let updateProfile = {
      password: bcrypt.hashSync(password, 10),
      username: username,
    };
    return await User.findByIdAndUpdate(userId, updateProfile, {
      new: true,
    })
      .then((result) => {
        res.send(true);
      })
      .catch((err) => {
        console.log(err);
        res.send(false);
      });
  } else {
    res.send({ message: "Password is not valid" });
  }
};
